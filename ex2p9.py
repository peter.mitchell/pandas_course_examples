import numpy as np
import pandas as pd

babynames = pd.Series(['Jathonathon', 'Zeltron', 'Ruger', 'Phreddy', 'Ruger', 'Chad', 'Chad',
                       'Ruger', 'Ryan', 'Ruger', 'Chad', 'Ryan', 'Phreddy', 'Phreddy', 'Phreddy',
                       'Mister', 'Zeltron', 'Ryan', 'Ruger', 'Ruger', 'Jathonathon',
                       'Jathonathon', 'Ruger', 'Chad', 'Zeltron'], dtype='string')

# My soln
chad = babynames=="Chad"
chad = chad.to_numpy().astype("bool")

chad = chad[chad]
print( len(chad) )

# Couse soln
print( babynames.value_counts().loc[["Chad","Ruger","Zeltron"]] )
