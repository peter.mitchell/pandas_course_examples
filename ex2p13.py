import numpy as np
import pandas as pd

coaches = pd.Series(['Aaron', 'Donald', 'Joshua', 'Peter', 'Scott', 'Stephen'], dtype='string')
print(coaches)

players = pd.Series(['Asher', 'Connor', 'Elizabeth', 'Emily', 'Ethan', 'Hannah', 'Isabella', 'Isaiah', 'James',
                     'Joshua', 'Julian', 'Layla', 'Leo', 'Madison', 'Mia', 'Oliver', 'Ryan', 'Scarlett', 'William',
                     'Wyatt'], dtype='string')
print(players)

coach_player_ind = np.array([ 0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 4, 5, 5, 5])
np.random.shuffle(coach_player_ind)
answer = pd.Series(players.to_numpy(), index=coaches.iloc[coach_player_ind])

print(answer)

### Course soln
print("")

coaches = coaches.sample(frac=1, random_state=2357) # This shuffles the order (but not relationship between index and value)
players = players.sample(frac=1, random_state=7532)
# np.ceil "ceiling", round up to nearest integer value
repeats = np.ceil(len(players)/len(coaches)).astype('int64')
# TIL in python if you multiply a list by a constant it just copies entries in the list
# i.e., [2,3]*2 = [2,3,2,3] # How did I not know this? lol
# .head(N) just returns the N first elements from the series
coaches_repeated = pd.concat([coaches] * repeats).head(len(players))
result = players.copy()
result.index = pd.Index(coaches_repeated, name='coach')
print(result)



