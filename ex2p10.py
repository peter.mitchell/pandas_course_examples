import numpy as np
import pandas as pd

# Note he wants to update based on order in bees array/ knees, i.e. not matching on Series index (which seems besides the point to me but ok)

bees = pd.Series([True, True, False, np.nan, True, False, True, np.nan])
print(bees)

knees = pd.Series([5,2,9,1,3,10,5,2], index = [7,0,2,6,3,5,1,4])
print(knees)

knees[bees.isna().to_numpy()] *= 2

print(knees)
