import numpy as np
import pandas as pd

pickle = pd.Series([1.5, np.nan, 2.3, np.nan, np.nan, -3.9, np.nan, 4.5, np.nan, np.nan, np.nan, 1.9, np.nan])
print(pickle)

# Course soln

# These are subsets of the more general fillna() method
prevs = pickle.ffill()
nexts = pickle.bfill()

print(prevs)
print(nexts)

min = pd.concat((prevs,nexts),axis=1).min(axis=1)

pickle.fillna(min,inplace=True)
print(pickle)
