import numpy as np
import pandas as pd

couples = pd.DataFrame({
    'person1': ['Cody', 'Dustin', 'Peter', 'Adam', 'Ryan', 'Brian', 'Jordan', 'Gregory'],
    'person2': ['Sarah', 'Amber', 'Brianna', 'Caitlin', 'Rachel', 'Kristen', 'Alyssa', 'Morgan']
}).convert_dtypes()
print(couples)
print("")

ages = pd.DataFrame({
    'person': ['Adam', 'Alyssa', 'Amber', 'Brian', 'Brianna', 'Caitlin', 'Cody', 'Dustin', 'Gregory', 'Jordan',
               'Kristen', 'Rachel', 'Morgan', 'Peter', 'Ryan', 'Sarah'],
    'age': [62, 40, 41, 50, 65, 29, 27, 39, 42, 39, 33, 61, 43, 55, 28, 36]
}).convert_dtypes()
print(ages)
print("")

df = pd.merge(left=couples, right=ages, left_on="person1", right_on="person")
df.rename(columns={"person2":"partner"},inplace=True)
df.drop(columns=["person1"],inplace=True)
#print(df)

df2 = pd.merge(left=couples,right=ages, left_on="person2", right_on="person")
df2.rename(columns={"person1":"partner"},inplace=True)
df2.drop(columns=["person2"],inplace=True)

#print(df2)

df3 = pd.merge(df,df2,left_on="person",right_on="partner")
#print(df3)
df4 = pd.merge(df2,df,left_on="person",right_on="partner")
#print(df4)

df5 = pd.concat([df3,df4],axis=0)

df5.drop(columns=["partner_y","person_y"],inplace=True)
df5.rename(columns={"age_y":"age_partner", "age_x":"age_person"},inplace=True)
df5.rename(columns={"person_x":"person", "partner_x":"partner"},inplace=True)

cradle_robbers = df5["age_person"] > df5["age_partner"] +20
cradle_robbers = cradle_robbers & (df5["age_partner"] < 30)

df5 = df5.loc[cradle_robbers,"person"]

print (df5)


# Course soln

print ("")
# Replace numbered index with person string as the indices
ages = ages.set_index('person').age
couples['age1'] = ages.loc[couples.person1].to_numpy()
couples['age2'] = ages.loc[couples.person2].to_numpy()
# This is a more elegant way of doing the merge technique I used

cr1 = couples.loc[(couples.age1 - couples.age2 >= 20) & (couples.age2 < 30), 'person1']
cr2 = couples.loc[(couples.age2 - couples.age1 >= 20) & (couples.age1 < 30), 'person2']
cradle_robbers = pd.concat((cr1, cr2))
print(cradle_robbers)
