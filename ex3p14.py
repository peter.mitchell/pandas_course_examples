import numpy as np
import pandas as pd

potholes = pd.DataFrame({
    'length':[5.1, np.nan, 6.2, 4.3, 6.0, 5.1, 6.5, 4.3, np.nan, np.nan],
    'width':[2.8, 5.8, 6.5, 6.1, 5.8, np.nan, 6.3, 6.1, 5.4, 5.0],
    'depth':[2.6, np.nan, 4.2, 0.8, 2.6, np.nan, 3.9, 4.8, 4.0, np.nan],
    'location':pd.Series(['center', 'north edge', np.nan, 'center', 'north edge', 'center', 'west edge',
                          'west edge', np.nan, np.nan], dtype='string')
})

print (potholes)

keep_row = potholes.isna().sum(axis=1) < 3
potholes1 = potholes.loc[keep_row]

av_column = potholes1.mean(axis=0)

replace_element = potholes1.isna()

# Got stuck


# Course soln
# Unlike me he computes mean/mode before actually dropping the last row
drop_rows = potholes.isnull().sum(axis=1) > potholes.shape[1]/2
# This fills in the NaNs for the columns where a mean can be defined (i.e. not the last col of strings)
potholes.fillna(potholes.mean(), inplace=True)

# "Non-numeric" in the question is referring to the column of strings at the end
# This nutcase likes using .location instead of ["location"] syntax to confuse the bejesus out of me (looks like a method lol)
# potholes.location.mode() returns the mode of the column "location"
# It returns this as a series though (with new index set to 0). Which means that if you do fillna it will try to fill NaNs in
#  the first element in "location", rather than all elements
# .iat[0] plucks out the mode string from the series (equivalent to .values[0])
potholes.location.fillna(potholes.location.mode().iat[0], inplace=True)

potholes = potholes.loc[~drop_rows]
print(potholes)
