import numpy as np
import pandas as pd

games = pd.DataFrame({
    'bella1':   ['2nd', '3rd', '1st', '2nd', '3rd'],
    'billybob': ['1st', '2nd', '2nd', '1st', '2nd'],
    'nosoup4u': ['3rd', '1st', '3rd', '3rd', '3rd'],
    'venue': ['desert', 'ocean', 'desert', 'ocean', 'desert']
})
print(games)

# First put the players into a single column
temp = games.melt(id_vars="venue",value_vars=["bella1","billybob","nosoup4u"],var_name="player",value_name="placement")

# Then group entries into unique values of venue, player, placement
temp2 = temp.groupby(["venue","player","placement"])

# Count how many times each entry occurs
temp3 = temp2.agg("size")

# Reshape answer to the desired format, with player,placement as a column multi-index
temp4 = temp3.unstack(level=("player","placement"))

print(temp4)



# Course soln
print("")
step1 = games.melt(id_vars='venue', var_name='player', value_name='placed')
# Turning into categorial seems to (only) result in the answer returning integers instead of floats
step1['venue'] = pd.Categorical(step1.venue)
step1['player'] = pd.Categorical(step1.player)
step1['placed'] = pd.Categorical(step1.placed)
step2 = step1.groupby(['venue', 'player', 'placed']).agg('size')
step3 = step2.unstack(level=['player','placed'])
print(step3)
