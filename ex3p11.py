import numpy as np
import pandas as pd

generator = np.random.default_rng(123)
df = pd.DataFrame({
    'john': generator.choice([True, False], size=10, replace=True),
    'judy': generator.choice([True, False], size=10, replace=True)
})
print(df)

party = df["john"] & df["judy"]
print (party)

# [::-1] reverses order (but remember pandas preserves the index in the resulting series)
# cumsum is exploiting behaviour that booleans can be regarded as 0 (False), 1 (True)
grps = party.iloc[::-1].cumsum()
# grps gives a unique index to each party and the days running up to it (note it is in reverse order atm, although indicies are preserved)
print (grps)

# The unique index means that we can now split the data into groups using .groupby()
# The returned object has a method cumcount (where we get back a series with same length as grps, 
# where for each unique group we do a cumsum on the number of elements)
# Note the order of this series is reverted back to the original order of df (I guess because the series is sorted by index before doing cumcount)
# The last trick is that by using ascending=False argument, this cumcount is done in reverse order for each group
# Which gives us the days until the party, (as opposed to the days since the last party I guess?)

print (party.groupby(grps).cumcount())

print (party.groupby(grps).cumcount(ascending=False))

df['days_til_party'] = party.groupby(grps).cumcount(ascending=False)

print (df)

# Finally, we need to handle the final group of entries, since there is no party at the end of the series :(
print ("")
print (party.loc[party].index[-1]) # Find the index of the final party

# Set days after that to NaN
df.loc[(party.loc[party].index[-1] + 1):, 'days_til_party'] = pd.NA

print (df)
