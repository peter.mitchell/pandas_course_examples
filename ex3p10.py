import numpy as np
import pandas as pd

couples = pd.DataFrame({
    'man': [
        ['fishing', 'biking', 'reading'],
        ['hunting', 'mudding', 'fishing'],
        ['reading', 'movies', 'running'],
        ['running', 'reading', 'biking', 'mudding'],
        ['movies', 'reading', 'yodeling']
    ],
    'woman': [
        ['biking', 'reading', 'movies'],
        ['fishing', 'drinking'],
        ['knitting', 'reading'],
        ['running', 'biking', 'fishing', 'movies'],
        ['movies']
    ]
})

print( couples )

# Convert lists to sets - "set" is a native python datatype - that (among other things presumably) can be
# used to find elements from one set that are not in the other
sets = couples.applymap(set)

print (sets)

# .diff is a pandas specific method, which subtracts one column from another (if axis=1)
# periods tells it to subtract from each column the next column behind it given by the shift periods
# so periods = 1 gives you col1 - Nothing (NaN, col2-col
# The trick is subtracting two sets gives you the elements in the first set that are not in the second
woman_not_man = sets.diff(axis=1, periods=1).drop(columns='man')
man_not_woman = sets.diff(periods=-1, axis=1).drop(columns='woman')

print (woman_not_man)

hobbies_not_shared = pd.concat((man_not_woman, woman_not_man), axis=1).applymap(list)

print (hobbies_not_shared)
