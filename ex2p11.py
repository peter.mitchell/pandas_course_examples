import numpy as np
import pandas as pd

asking_prices = pd.Series([5000, 7600, 9000, 8500, 7000], index=['civic', 'civic', 'camry', 'mustang', 'mustang'])
print(asking_prices)

fair_prices = pd.Series([5500, 7500, 7500], index=['civic', 'mustang', 'camry'])
print(fair_prices)


all_fair_prices = fair_prices.loc[asking_prices.index]
print(all_fair_prices)

yep = asking_prices < all_fair_prices

answer = np.where( yep.to_numpy() )[0]
print( answer )


# Course soln used an extra method reset_index()
# Not completely obvious to me what this does..
# With the drop argument set to True, we lose the car names as indicies

print( "" )
print( yep )
print( yep.reset_index() )
print( yep.reset_index(drop=True) )
