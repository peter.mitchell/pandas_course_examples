import numpy as np
import pandas as pd

generator = np.random.default_rng(314)

sales = pd.DataFrame({
    'date':pd.date_range(start = '2020-01-01', periods=5).repeat(2),
    'store_id':np.tile([1,2], 5),
    'sales1':np.round(generator.normal(loc=750, scale=20, size=10), 2),
    'sales2':np.round(generator.normal(loc=650, scale=40, size=10), 2),
    'members':generator.integers(low=20, high=25, size=10)
})
sales.loc[sales.store_id == 2, 'sales1'] = np.nan
sales.loc[sales.store_id == 1, 'sales2'] = np.nan
print(sales)

# My haggard soln
# Note my soln is haggard because it doesn't generalise to larger numbers of stores
sales_1 = sales[sales["store_id"]==1]
sales_1.drop(["store_id","sales2"],axis=1,inplace=True)
sales_1.rename(columns={'members':'members1'}, inplace=True)

sales_2 = sales[sales["store_id"]==2]
sales_2.drop(["store_id","sales1"],axis=1,inplace=True)
sales_2.rename(columns={'members':'members2'}, inplace=True)

soln = pd.merge(sales_1,sales_2,on="date")
#print( soln)


### Course soln 1#########
# Split sales/members entries based on store_id into different columns, retaining a row for each unique date
# dropna kills the columns that are full are of nan values
sol1 = sales.pivot(index='date', columns='store_id').dropna(axis=1)
#print(sol1)
# This complex lines does nothing to the data entries, just reworks the row/column indexing names and also goes from multi-col index to single col index
'''print ("")
for x,y in sol1.columns:
    print(x,y)
    print(f'{x[:-1]}_{y}' if x[-1] in ('1', '2') else f'{x}_{y}')
print ("")
quit()'''
sol1.columns = [f'{x[:-1]}_{y}' if x[-1] in ('1', '2') else f'{x}_{y}' for x,y in sol1.columns]
#print(sol1)


### Course soln 2 ##########

print ( sales.set_index(['date','store_id']) )
# The unstack takes the second row index of the row multi-index, and makes that a column index instead
print ( sales.set_index(['date','store_id']).unstack() )
# Dropna kills nan columns as before
sol2 = sales.set_index(['date','store_id']).unstack().dropna(axis=1)

# Reformatting , get rid of column multi-index
sol2.columns = [f'{x[:-1]}_{y}' if x[-1] in ('1', '2') else f'{x}_{y}' for x,y in sol2.columns]
#print(sol2)
