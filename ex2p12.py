import numpy as np
import pandas as pd

generator = np.random.default_rng(123)
beef_prices = pd.Series(
    data = np.round(generator.uniform(low=3, high=5, size=10), 2),
    index = generator.choice(10, size=10, replace=False)
)
print(beef_prices)

# Pandas seems to be less clever than I thought about applying operations on an index-by-index basis, so we have to reorder the series in the course soln
beef_prices.sort_index(inplace=True)

# This does not work - since the subtraction is done on pandas index values, which haven't changed when we do the slice
#diff = beef_prices.iloc[1:] - beef_prices.iloc[0:-1]
# This works
diff = beef_prices.iloc[1:].to_numpy() - beef_prices.iloc[0:-1].to_numpy()
print (diff)
print (np.argmax(diff)+1)

# Course soln
# This works unlike first example because now the beef_prices_prev indices are consistent with beef_prices
beef_prices_prev = beef_prices.shift(periods=1)

diff = beef_prices - beef_prices_prev
print ("")
print (diff)
print (diff.idxmax())
