import numpy as np
import pandas as pd

pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)

generator = np.random.default_rng(1357)
artists = ['Mouse Rat', 'Binary Brothers', 'Lady Gigabyte', 'The Rolling Sums']
venues = ['Super Dome', 'STAPLES Center', 'Madison Square Garden']

concerts = pd.DataFrame({
    'artist':generator.choice(['Mouse Rat', 'Binary Brothers', 'Lady Gigabyte', 'Just-in-Timeberlake'], size=200, replace=True),
    'venue':generator.choice(venues + ['Red Rocks'], size=200, replace=True),
    'date':pd.to_datetime('2020-01-01') + pd.to_timedelta(generator.choice(365, size=200, replace=True), unit='D'),
}).drop_duplicates().convert_dtypes()
print(concerts)


# First find all the unique artist/venue combinations
# This is effectively a cross product of artist and venue series (as recommended in the question instructions)
combos = pd.merge(concerts["artist"],concerts["venue"],on=np.ones_like(concerts.index))[["artist","venue"]]
combos.drop_duplicates(inplace=True)

combos["artist"] += " " + combos["venue"]
combos.drop(["venue"],axis=1,inplace=True)
combos.rename(columns={"artist":"artist venue"},inplace=True)

'''concerts["artist"] += " " + concerts["venue"]
concerts.drop(["venue"],axis=1,inplace=True)
concerts.rename(columns={"artist":"artist venue"},inplace=True)
print(concerts)'''

# At this point I don't know how to group datetime elements by month..

######### Course soln ##################

# First compute the unique artist/venue combinations as a multi-index
# "Cross-product" comes in from the "from_product" method
artist_venues = pd.MultiIndex.from_product([artists, venues], names = ['artist', 'venue'])

# Then we use this multi-index by setting it as the index of the concerts DFrame
# So no need to combine the artist/venue columns like I started (though that would work also!)
# Also no need to groupby artist/venue, we just make these the index (multi index in this case since there are two variables)
concerts2 = pd.merge(
    left=pd.DataFrame(index=artist_venues),
    right=concerts.set_index(['artist', 'venue']),
    on=['artist', 'venue'],
    how='left'
)

print(artist_venues)
print(concerts2)

# Ok here is the method for datetime element to year/month
concerts2['yearmonth'] = concerts2.date.dt.to_period('M')

# Go from "narrow" to "wide" format i.e. make artist/venue combos the columns, while applying the aggfunc count to deal with non-unique elements
# We get non-unique elements since we make the year/month the index
# Recall that pivot table is the "narrow">"wide" method that can deal with duplicate entries ("aggfunc"), and missing entries ("fill_value")
concerts2.pivot_table(
    index='yearmonth',
    values='date',
    columns=['artist', 'venue'],
    aggfunc='count',
    fill_value=0,
    dropna=False
)

print(concerts2)
