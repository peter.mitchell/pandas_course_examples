import numpy as np
import pandas as pd

df = pd.DataFrame({
    'A': ['foo', 'bar', 'foo', 'bar', 'bar', 'foo', 'foo'],
    'B': [False, True, False, True, True, True, True],
    'C': [2.1, 1.9, 3.6, 4.0, 1.9, 7.8, 2.8],
    'D': [50, np.nan, 30, 90, 10, np.nan, 10]
}).convert_dtypes()
print(df)


print (df.groupby('A')['C'].transform(lambda x: x.sort_values()))
