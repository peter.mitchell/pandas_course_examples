import numpy as np
import pandas as pd

generator = np.random.default_rng(2718)
persons = pd.DataFrame({
    'id':     [ 2,  3,     8, 12, 14, 15,    17,    32,    35,    41,    60, 64, 83, 98],
    'mom_id': [35, 41, pd.NA, 35, 41,  2, pd.NA, pd.NA, pd.NA, pd.NA,     8, 12, 35,  2],
    'dad_id': [17,  8, pd.NA, 17,  8, 32, pd.NA, pd.NA, pd.NA, pd.NA, pd.NA, 14, 17, 14],
    'IQ': np.round(generator.normal(loc=100, scale=20, size=14))
})
print(persons)

# My soln, first thought is to use ms.match_searchsorted
# To compute a column of dad_iq and mom_iq for each person
# I'm 90% sure this would work, but can't be bothered to type this out

# ptr = ms.match( id, dad_id)
# ok_match = ptr>= 0
# dad_iq = np.zeroes_like(id)
# dad_iq[ok_match] = IQ[ptr][ok_match]

# And so on...

# Ah I've neglected the possibility that there are children (that's fine since we just add a child_iq column)
# And there are also siblings - this one is a bit different - but can be done using a tiny bit of extra boolean logic I believe
# i.e. don't count yourself as a sibling.
# I guess there are also aunts/uncles to consider hmm

# Edit - there is one point of extra nastiness, which is that you can have more than one sibling
# Which I think may indeed break the ptr method if I'm not mistaken

# Course soln (hefty this time)

# This is just another way of doing a ptr match
moms = pd.merge(
    left=persons[['id', 'mom_id']],
    right=persons[['id', 'IQ']],
    how='inner',
    left_on='mom_id',
    right_on='id',
    suffixes=['', '_relative']
)

# Same
dads = pd.merge(
    left=persons[['id', 'dad_id']],
    right=persons[['id', 'IQ']],
    how='inner',
    left_on='dad_id',
    right_on='id',
    suffixes=['', '_relative']
)
# Same (except this time we match on two ids - for this I would need to define a unique parents_index for ptr method)
sibs = pd.merge(
    left=persons[['id', 'mom_id', 'dad_id']].dropna(),
    right=persons[['id', 'mom_id', 'dad_id', 'IQ']],
    how='inner',
    on=['mom_id', 'dad_id'],
    suffixes=['', '_relative']
)
# As suspected - don't count yourself as a relative
sibs = sibs.loc[~(sibs.id == sibs.id_relative)]

# Note that unlike ptr, this pd.merge technique has the big advantage that you can have multiple matches
# I.e. multiple siblings that 

# Build an array of the IQ of each child (cut)
children = pd.concat((
    persons[['dad_id', 'id', 'IQ']].dropna().rename(columns={'dad_id':'id', 'id':'id_relative'}),
    persons[['mom_id', 'id', 'IQ']].dropna().rename(columns={'mom_id':'id', 'id':'id_relative'})
))

#print(children)
#quit()

# Finally, we put together a giant dataframe that gives the IQ of every single relative of each person
relatives = pd.concat((
    moms[['id', 'id_relative', 'IQ']],
    dads[['id', 'id_relative', 'IQ']],
    sibs[['id', 'id_relative', 'IQ']],
    children[['id', 'id_relative', 'IQ']]
))

#print(relatives)
#quit()

# Note it turns out everyone has at least one relative in this example
# Note also that this spits out a series with "id" as the series index
avgrelatveIQs = relatives.groupby('id')['IQ'].mean()

#print( avgrelatveIQs)
#quit()

# Set common index as avgrelatveIQs
persons.set_index('id', inplace=True)
persons['AvgRelIQ'] = avgrelatveIQs
persons['FamilyIQ'] = 0.5 * persons.IQ + 0.5 * persons.AvgRelIQ

winner = persons.FamilyIQ.idxmax()

print(winner)
