import numpy as np
import pandas as pd

generator = np.random.default_rng(seed=1234)
classes = ['english', 'math', 'history', 'chemistry', 'gym', 'civics', 'writing', 'engineering']

schedules = pd.DataFrame({
        'student_id':np.repeat(np.arange(100), 4),
        'class':generator.choice(classes, size=400, replace=True)
        }).drop_duplicates()

schedules['grade'] = generator.integers(101, size=schedules.shape[0])

print(schedules)

######### My Soln ####################

chem_ind = np.where( (schedules["class"] == "chemistry").to_numpy())[0]
b4_ind = chem_ind-1

# Check if each student actually has a class before chemistry
ok = schedules["student_id"].iloc[chem_ind].to_numpy() == schedules["student_id"].iloc[b4_ind].to_numpy()
b4_ind = b4_ind[ok]
chem_ind = chem_ind[ok]

temp = schedules.iloc[chem_ind]
temp["class_before"] = schedules["class"].iloc[b4_ind].to_numpy()
temp2 = temp.groupby("class_before")

print ("")
print( temp2.agg({"grade":["mean","median","count"]}) )

######## Course soln ###################

# Note this returns NaN if there is not a prior class to the current class for each student
schedules['prev_class'] = schedules.groupby('student_id')['class'].transform(pd.Series.shift)

class_pairs = schedules.groupby(['prev_class', 'class']).agg(
    students = ('student_id', 'count'),
    avg_grade = ('grade', 'mean'),
    med_grade = ('grade', 'median')
)
print( class_pairs.xs(key='chemistry', axis=0, level=1, drop_level=False).sort_values('med_grade') )
