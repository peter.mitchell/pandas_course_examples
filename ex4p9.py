import numpy as np
import pandas as pd

pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)

generator = np.random.default_rng(90)
products = ['iev','pys','vae','dah','yck','axl','apx','evu','wqv','tfg','aur','rgy','kef','lzj','kiz','oma']
hits = pd.DataFrame({
    'visitor_id':generator.choice(5, size=20, replace=True) + 1,
    'session_id':generator.choice(4, size=20, replace=True),
    'date_time':pd.to_datetime('2020-01-01') + pd.to_timedelta(generator.choice(60, size=20), unit='m'),
    'page_url':[f'shoesfordogs.com/product/{x}' for x in generator.choice(products, size=20, replace=True)]
})
hits['session_id'] = hits.visitor_id * 100 + hits.session_id
print(hits)

#abs(hits["date_time"].iloc[1] - hits["date_time"].iloc[0]) < pd.Timedelta(value=5, unit='minutes')

'''def test(x):
    print("")
    print ("JIMBO")
    print(x)
    print("")
    print(x.sort_values)
    print("")
    return x.sort_values()'''

#ok so this is actually working, what it does is preserve the visitor_id order, but for elements with common visitor_id
# it then shuffles those elements such that the lowest value appears first.
#print ( hits.groupby('visitor_id')["date_time"].transform(test))



### Course soln #########
# First sort the dataframe first by visitor ID, and then by date_time (this is admitedly much cleaner than to do with numpy!)
# Note I'm not sure this actually has any purpose
# Edit: its just for easier to read formatting for the print statement at the very end 
hits.sort_values(by=['visitor_id', 'date_time'], inplace=True)
#print (hits)

# For each unique visitor_id and session_id set, compute the min/max times
# i.e. a common visitor_id and session_id means a single browsing session, and so should be merged together , irrespective of the times
sessions = hits.groupby(['visitor_id', 'session_id']).agg(
    date_time_min=('date_time', 'min'),
    date_time_max=('date_time', 'max')
)
#print (sessions)

# Now sort first by visitor_id, then by date_time_min
sessions.sort_values(['visitor_id', 'date_time_min'], inplace=True)
#print (sessions)

sessions['date_time_max_p5'] = sessions.date_time_max + pd.Timedelta(minutes=5)
#print (sessions)

# This is a weird line, lets see
sessions['date_time_max_p5_cummax'] = sessions.groupby('visitor_id')['date_time_max_p5'].cummax()
#print (sessions)

# This seems to be a weird way of comparing (for a given row) date_time_min against date_time_max+5mins from the previous row 
# The weird cummax part was that its possible for a session that starts earlier to end later than the one after it
# So in this case, we compare date_time_min against the date_time_max+5mins of whichever earlier starting session finished last
sessions['date_time_max_p5_cummax_prev'] = sessions.groupby('visitor_id')['date_time_max_p5_cummax'].transform(pd.Series.shift)
#print (sessions)

# Makes sense I think
sessions['group_with_prev'] = sessions.date_time_min <= sessions.date_time_max_p5_cummax_prev

# Cute :)
sessions['session_group_id'] = (~sessions.group_with_prev).cumsum()
#print (sessions)

hits = pd.merge(left=hits, right=sessions[['session_group_id']], on='session_id')
print(hits)
