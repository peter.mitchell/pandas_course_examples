import numpy as np
import pandas as pd

descriptions = pd.Series([
    "soft and fuzzy teddy bear, product dims: 1'x2'x1', shipping not included",
    "birch table measures 3'x6'x2'",
    "tortilla blanket ~ sleep like a fajita ~ 6'x8'x1'",
    "inflatable arm tube man | 12'x1'x1' when inflated",
    "dinosaur costume -- 6'x4'x2' -- for kids and small adults"
], dtype='string')

print(descriptions)
print ("")
#print(descriptions.str.split("'"))

split = descriptions.str.split("'")

dim1 = split.str.get(0)
dim1 = dim1.str.split(" ").str.get(-1)

dim2 = split.str.get(1)
dim2 = dim2.str.split("x").str.get(1)

dim3 = split.str.get(2)
dim3 = dim3.str.split("x").str.get(1)

products = pd.DataFrame( {"descriptions":descriptions, "dim1":dim1.astype("float"), "dim2":dim2.astype("float"), "dim3":dim3.astype("float")} )

products["volume"] = products.dim1 * products.dim2 * products.dim3

print(products)

########### Course solution 1 (w. regular expressions) #########
print("")

dims = descriptions.str.extract(r"(\d+)'x(\d+)'x(\d+)'").astype('int64')
dims.product(axis=1)

# More general solution (could handle 1002 dimensional objects for example)
########### Course solution 2 (also w. regular expressions) #########
print("")

dims = descriptions.str.extractall(r"(\d+)'").astype('int64')

print(dims)
dims.groupby(dims.index.get_level_values(0)).agg(pd.Series.product)

