import numpy as np
import pandas as pd

generator = np.random.default_rng(5555)
regions = ['north', 'south', 'east', 'west']

commercials = pd.DataFrame({
    'commercial_id': range(10),
    'region': generator.choice(regions, size=10),
    'date_time': pd.to_datetime('2020-01-01') + pd.to_timedelta(generator.integers(240, size=10), unit='h')
})
print(commercials)

sales = pd.DataFrame({
    'sale_id': range(10),
    'region': generator.choice(regions, size=10),
    'date_time': pd.to_datetime('2020-01-01') + pd.to_timedelta(generator.integers(240, size=10), unit='h'),
    'revenue': np.round(generator.normal(loc=20, scale=5, size=10), 2)
})
print(sales)

# Ok I don't finish this - but you could do this basically with a for loop - as you see with the horrible example I lay out here

#print(commercials.groupby("region").agg(list))
#print(sales.groupby("region").agg(list))

mess = pd.merge(commercials.groupby("region").agg(list), sales.groupby("region").agg(list), on="region")

def test(x):
    com_id = x[0]
    dt_com = x[1]
    dt_sale = x[2]
    val_sale = x[3]
    
    #print("Hello",x[0],x[1])
    return 0

mess[["commercial_id","date_time_x","date_time_y","revenue"]].apply(lambda x: test(x),axis=1)


##### Course soln #############
commercials.sort_values('date_time', inplace=True)
sales.sort_values('date_time', inplace=True)

# This explots the merge_asof function. The "_asof" means that we merge by the closest possible join variable value, 
# rather than by exactly matching values - pretty cool!

# So this is saying we merge commercials (right) onto sales (left)

# We are going to do a match on the "date_time" variable, finding the closest commerical date_time for each date_time in sales

# But first we also say that we only want to match date_times if they are in the same region (by="region")

# Finally, the "direction" argument is special for pd.merge_asof(),
# you can specify "nearest", "backward" (default), "forward", which will match on the nearest date time, the date_time that 
# of the two closest is the first to appear in the sales array, or the date time of the two closest that is the second to appear in the sales array

# In this case since commercials and sales have been sorted in the first step, we use backward because we want the sales
# that occured just after the commerical aired

# Note finally that if no commercial aired before a revenue report in a given region, we get NaN

soln = pd.merge_asof(left=sales, right=commercials, on='date_time', by='region', direction='backward')

print(soln)
