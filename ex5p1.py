import numpy as np
import pandas as pd

# exposed places
exposed = [0,5,9]

# whereabouts of each person
generator = np.random.default_rng(2468)
Nplaces = 10
Npersons = 10
place_ids = np.arange(Nplaces)
visits = generator.choice(place_ids, size=3*Nplaces, replace=True)
split_idxs = np.sort(generator.choice(len(visits), size=9, replace=True))
whereabouts = pd.DataFrame({
    'person_id': range(Npersons),
    'places': [np.unique(x).tolist() for x in np.array_split(visits, split_idxs)]
})
print(whereabouts)
print("")
print("Exposed places: ", exposed)

# My attempt (using np.searchsorted)
print("")

ptr = np.searchsorted(exposed, whereabouts["places"].loc[0])

ptr[ptr>=len(exposed)] = 0
ptr[ptr<0]          = 0

# Return -1 where no match is found
ind  = np.array(exposed)[ptr] != whereabouts["places"].loc[0]
ptr[ind] = -1

# Put ptr back into original order                                                                                                 
idx = slice(0,len(exposed))
ind = np.arange(len(exposed))[idx]
ptr = np.where(ptr>= 0, ind[ptr], -1)

ok_match = ptr>=0

exposed_0 = np.array(exposed)[ptr][ok_match]

# Soln for a single row using match_searchsorted
print(whereabouts["places"].loc[0])
print(exposed_0)

# Here is how to do this (stealing method from course soln) for each row in turn.
# Note this is probably using a sneaky for loop lol - so I'm guessing won't scale well for many rows

def ms(arr1, arr2):
    ptr = np.searchsorted(arr2, arr1)

    ptr[ptr>=len(arr2)] = 0
    ptr[ptr<0]          = 0

    ind  = np.array(arr2)[ptr] != arr1
    ptr[ind] = -1
    
    idx = slice(0,len(arr2))
    ind = np.arange(len(arr2))[idx]
    ptr = np.where(ptr>= 0, ind[ptr], -1)

    ok_match = ptr>=0

    return arr1[ok_match]

whereabouts['exposures_1'] = whereabouts["places"].apply(lambda x: ms(np.array(x), np.array(exposed)))
#print ( whereabouts )

# Course soln 1
# Uses the unique features of python "sets" to locate elements from one list that are in another
# I wonder how efficient this is?
# In any case, similar to np.isin this is less powerful than np.searchsorted (see comments for second course soln below for explanation)
# But to be fair we don't need the extra np.searchsorted information for this problem
'''print("")
set1 = set( whereabouts["places"].iloc[0] )
set2 = set(exposed)
print(set1,set2)
print(set1&set2)
quit()'''

whereabouts['exposures'] = whereabouts['places'].apply(lambda x: list(set(x) & set(exposed)))
print(whereabouts)

# Course soln 2
# This one is using the numpy function np.isin() (which is a similar but less powerful version of numpy.searchsorted)
# (since it doesn't tell you where the matched elements appear in the other array)
# Not sure the apply method is not used in this case, would seem more straightforward - but there is probably a good reason
print("")
expanded = whereabouts.explode(column='places')
#print(expanded)
filtered = expanded.loc[expanded.places.isin(exposed)]
# Ok cool, so one thing for .agg method is to turn the common person_id elements into a list (instead of as a mean or similar)
aggregated = filtered.groupby('person_id')[['places']].agg(list)
aggregated.rename(columns={'places':'exposures'}, inplace=True)
whereabouts = pd.merge(left=whereabouts, right=aggregated, how='left', on='person_id')
whereabouts['exposures'] = whereabouts.exposures.apply(lambda x: x if isinstance(x, list) else [])
