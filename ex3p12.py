import numpy as np
import pandas as pd

machine_products = pd.DataFrame({
    'machine': ['abc', 'abc', 'def', 'def', 'def', 'ghi'],
    'product': ['skittles', 'soap', 'soap', 'm&ms', 'skittles', 'm&ms'],
    'stock': [10, 0, 15, 2, 0, 3]
})
print(machine_products)


# My soln

prod_u = machine_products["product"].unique()
df = pd.DataFrame(index=prod_u)

ones = np.ones(len(machine_products["stock"])).astype("int")
machine_products["ones"] = ones

out_of_stock = machine_products["stock"] == 0
machine_products["out_of_stock"] = out_of_stock

df[["stock","n_machine","out_of_stock"]] = machine_products.groupby(machine_products["product"]).sum()


print ("")
print (df)


# Course soln
print ("")

def count_zero(x): return (x == 0).sum()

# Ok so it seems the (complex) syntax is for each set of elements of a given product
# Return a number that applies the function to "count" (or count_zero()) to those elements from column "stock"
# And assign that number to a new row given by the product name, and a column called "n_machines", or "n_machines_empty"
# Oof this is compact but extremely implicit syntax! Powerful but badly written out (probably would look better with explicit argument names)

df = machine_products.groupby('product').agg(
    n_machines = ('stock', 'count'),
    n_machines_empty = ('stock', count_zero)
)

print ("")
print (df) 
